<?php

/******************************************************************************
 *
 * Extension Manager/Repository config file for ext "magnificpopupvideo".
 *
 ******************************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Magnificpopup Video',
	'description' => 'A video element based on Magnificpopup by Dimitry Semenov/Danny Hearnah - http://dimsemenov.com/plugins/magnific-popup/',
	'category' => 'frontend',
	'author' => 'Stefan Padberg',
	'author_email' => 'post@bergische-webschmiede',
	'author_company' => 'Bergische Webschmiede',
	'state' => 'alpha',
	'uploadfolder' => true,
	'createDirs' => '',
	'clearcacheonload' => true,
	'version' => '0.6.26',
	'constraints' => array (
		'depends' => array (
			'typo3' => '7.6.0-9.5.99',
		),
		'conflicts' => array (
		),
		'suggests' => array (
		),
	),
);
