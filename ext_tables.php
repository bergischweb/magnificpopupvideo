<?php
if( !defined('TYPO3_MODE') ) {
    die ('Access denied.');
}




/**
 * Default TypoScript
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY,
    'Configuration/TypoScript',
    'Magnificpopup Video TS Setup'
);

/**
 * Default Page TypoScript
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $_EXTKEY,
    'Configuration/PageTs/setup.tsconfig',
    'Magnificpopup Video PageTS'
);
