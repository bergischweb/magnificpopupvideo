<?php
if( !defined('TYPO3_MODE') ) {
	die ('Access denied.');
}

// Add the CType "magnificpopupvideo"
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
	'tt_content',
	'CType',
	[
		'LLL:EXT:magnificpopupvideo/Resources/Private/Language/locallang_db.xlf:magnificpopupvideo.title',
		'magnificpopupvideo',
		'tx-magnificpopupvideo-icon'
	],
	'textmedia',
	'after'
);

// Configure the default backend fields for the content element "magnificpopupvideo"
$GLOBALS['TCA']['tt_content']['types']['magnificpopupvideo'] = [
    'showitem' => '
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.header;header,
            subheader;LLL:EXT:magnificpopupvideo/Resources/Private/Language/locallang_db.xlf:label.videoTitle,
            bodytext;LLL:EXT:magnificpopupvideo/Resources/Private/Language/locallang_db.xlf:label.videoDescription,
        --div--;LLL:EXT:magnificpopupvideo/Resources/Private/Language/locallang_db.xlf:tabs.image,image,
        
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
        --div--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended
    ',
    'columnsOverrides' => [
        'bodytext' => [
            'defaultExtras' => 'richtext:rte_transform[mode=ts_css]'
        ]
    ]
];

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['magnificpopupvideo'] = "tx-magnificpopupvideo-icon";