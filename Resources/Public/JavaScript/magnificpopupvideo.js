/*
 *
 */
jQuery(document).ready( function($) {
    if( $('.magnificpopupvideo-container').length > 0 ) {
        $('.magnificpopupvideo-container').each( function() {
            var id = $(this).attr('id');
            $(this).find('a.magnificpopupvideo').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false
            });
        });
    }
});
