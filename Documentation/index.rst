# Introduction

## What does it do?

This extension provides a plugin to integrate videos in "popup mode",
i.e. the video will be represented by a screenshot. By clicking onto
it a lightbox pops up which contains the video.
