<?php
if( !defined('TYPO3_MODE') ) {
    die ('Access denied.');
}

// register icon
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'tx-magnificpopupvideo-icon',
    \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
    ['source' => 'EXT:magnificpopupvideo/Resources/Public/Icons/icon-magnificpopupvideo.png']
);
